import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Organization } from 'src/organizations/entities/organization.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StatisticsService {
  constructor(
    @InjectRepository(Organization)
    private orgRepository: Repository<Organization>,
  ) { }

  OrgStatistics() {
    return this.orgRepository.query(`
    SELECT 
    o.id,
    o.name,
    COUNT(DISTINCT  p.* )::INT AS projects,
    COUNT(DISTINCT  t.*)::INT AS tasks
    FROM 
    organization o
    LEFT JOIN projects p
    ON o.id = p.org_id AND o.deleted_at iS NOT null AND p.deleted_at is NOT NULL

    LEFT JOIN task t
    ON t.project_id = p.id AND t.deleted_at iS NOT NULL and p.deleted_at is NOT NULL
    
    GROUP BY o.id 
    `);
  }

  projectStatistics() {
    return this.orgRepository.query(`
    SELECT 
    p.id AS project_id,
    COUNT(DISTINCT  t.*) ::INT AS task_count,
    o.name AS org_name
    FROM projects p
    LEFT JOIN task t
    ON p.id = t.project_id

    LEFT JOIN organization o
    ON p.org_id = o.id
    GROUP BY p.id, o.name
    ORDER BY p.id asc
    
    `);
  }

  async allStatistics() {
    const allStat = await this.orgRepository.query(`
    SELECT 
    (
    SELECT 
    COUNT(o.id) AS org
    FROM
    organization o) AS org_count,
     (
    SELECT 
    COUNT(p.id) AS org
    FROM
    projects p) AS project_count,
     (
    SELECT 
    COUNT(t.id) AS org
    FROM
    task t) AS task_count
   
    `);

    return allStat[0];
  }
}
