import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrganizationsUserService } from 'src/organizations_user/organizations_user.service';
import { ProjectsService } from 'src/projects/projects.service';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private taskRepository: Repository<Task>,

    @Inject(UsersService)
    private usersService: UsersService,

    @Inject(ProjectsService)
    private projectsService: ProjectsService,

    @Inject(OrganizationsUserService)
    private organizationsUserService: OrganizationsUserService,
  ) { }

  async create(createTaskDto: CreateTaskDto): Promise<{ id: number }> {
    const userFound = await this.usersService.findOne(
      createTaskDto.created_by,
    );
    const projectFound = await this.projectsService.findOne(
      createTaskDto.project_id,
    );

    const orgUserFound = await this.organizationsUserService.findOne(
      createTaskDto.worker_user_id,
    );

    if (!Object.keys(projectFound)[0])
      throw new BadRequestException('project id not found');

    if (!Object.keys(orgUserFound)[0])
      throw new BadRequestException('worker_user_id id not found');


    if (!Object.keys(userFound)[0])
      throw new BadRequestException('created_by id not found');

    try {
      const newTask = await this.taskRepository
        .createQueryBuilder()
        .insert()
        .into('task')
        .values({
          ...createTaskDto,
          due_date: new Date(createTaskDto.due_date),
        })
        .returning('id')
        .execute();

      return { id: newTask.raw[0].id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findAll(): Promise<Task[]> {
    return await this.taskRepository
      .createQueryBuilder('task')
      .where('deleted_at = :deleted_at', { deleted_at: false })
      .getMany();
  }

  async findOne(id: number): Promise<{}> {
    const findTask = await this.taskRepository
      .createQueryBuilder('task')
      .where('task.id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .getOne();

    if (!findTask) return {};

    return findTask;
  }

  async update(
    id: number,
    updateTaskDto: UpdateTaskDto,
  ): Promise<{ id: number }> {
    const findTask = await this.taskRepository
      .createQueryBuilder('task')
      .where('task.id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .getOne();
    if (!findTask) throw new NotFoundException('Task with user_id not exists!');

    try {
      this.taskRepository
        .createQueryBuilder()
        .update('task')
        .set({
          ...updateTaskDto,
          due_date: updateTaskDto.due_date
            ? new Date(updateTaskDto.due_date)
            : findTask.due_date,
        })
        .where('id = :id', { id })
        .execute();
      return { id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async remove(id: number): Promise<{ id: number }> {
    const taskFound = this.findOne(id)
    if (!Object.keys(taskFound)[0]) throw new BadRequestException("id not found")
    await this.taskRepository
      .createQueryBuilder()
      .update('users')
      .set({ deleted_at: true })
      .where('id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .execute();
    return { id };
  }


}
