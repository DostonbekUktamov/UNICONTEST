import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrganizationsService } from 'src/organizations/organizations.service';
import { Project } from 'src/projects/entities/project.entity';
import { Task } from 'src/tasks/entities/task.entity';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { CreateOrganizationsUserDto } from './dto/create-organizations_user.dto';
import { UpdateOrganizationsUserDto } from './dto/update-organizations_user.dto';
import { OrganizationsUser } from './entities/organizations_user.entity';

@Injectable()
export class OrganizationsUserService {
  constructor(
    @InjectRepository(OrganizationsUser)
    private orgUserRepository: Repository<OrganizationsUser>,

    @InjectRepository(Project)
    private projectRepository: Repository<Project>,

    @Inject(UsersService)
    private usersService: UsersService,

    @Inject(OrganizationsService)
    private organizationsService: OrganizationsService,

  ) { }

  async create(
    createOrganizationsUserDto: CreateOrganizationsUserDto,
  ): Promise<{ id: number }> {
    const createduserFound = await this.usersService.findOne(
      createOrganizationsUserDto.created_by,
    );

    const userFound = await this.usersService.findOne(
      createOrganizationsUserDto.user_id,
    );

    const org = await this.organizationsService.findOne(
      createOrganizationsUserDto.created_by,
    );

    if (!Object.keys(createduserFound)[0])
      throw new BadRequestException('created_by id not found');

    if (!Object.keys(userFound)[0])
      throw new BadRequestException('user_id id not found');


    if (!Object.keys(org)[0])
      throw new BadRequestException('org_id id not found');

    try {
      const newUser = await this.orgUserRepository
        .createQueryBuilder()
        .insert()
        .into('organization_user')
        .values({ ...createOrganizationsUserDto })
        .returning('id')
        .execute();

      return { id: newUser.raw[0].id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findAll(): Promise<OrganizationsUser[]> {
    return await this.orgUserRepository
      .createQueryBuilder('organization_user')
      .where('deleted_at = :deleted_at', { deleted_at: false })
      .getMany();
  }

  async findOne(id: number): Promise<{}> {
    const findUser = this.orgUserRepository
      .createQueryBuilder('organization_user')
      .where('id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .getOne();

    if (!findUser) return {};

    return findUser;
  }

  async update(
    id: number,
    updateOrganizationsUserDto: UpdateOrganizationsUserDto,
  ): Promise<{ id: number }> {
    const findUserOrg = await this.findOne(id);
    if (!findUserOrg)
      throw new NotFoundException('OrganizationsUser with user_id not exists!');

    const createduserFound = await this.usersService.findOne(
      updateOrganizationsUserDto.created_by,
    );

    const userFound = await this.usersService.findOne(
      updateOrganizationsUserDto.user_id,
    );

    const org = await this.organizationsService.findOne(
      updateOrganizationsUserDto.created_by,
    );

    if (!Object.keys(createduserFound)[0])
      throw new BadRequestException('created_by id not found');

    if (!Object.keys(userFound)[0])
      throw new BadRequestException('user_id id not found');


    if (!Object.keys(org)[0])
      throw new BadRequestException('org_id id not found');

    try {
      this.orgUserRepository
        .createQueryBuilder()
        .update('organization_user')
        .set({ ...updateOrganizationsUserDto })
        .where('id = :id', { id })
        .execute();

      return { id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async remove(id: number): Promise<{ id: number }> {

    const orgUser = await this.findOne(id)
    if (Object.keys(orgUser)[0]) throw new BadRequestException("id not found")

    await this.orgUserRepository
      .createQueryBuilder()
      .update('organization_user')
      .set({ deleted_at: true })
      .where('id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .execute();
    return { id };
  }

  async orgUserWithTask(): Promise<Project[]> {
    return await this.projectRepository.query(`
    select 
     t.status,
     json_agg(DISTINCT ou.*) as "orgUser"
    from task t
    left join organization_user ou
    on t.worker_user_id = ou.id
    group by t.id
    `);
  }
}
