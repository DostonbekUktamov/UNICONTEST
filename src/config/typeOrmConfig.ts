import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConf = new DocumentBuilder()
    .setTitle('UniconTest')
    .setDescription('The Unicon Soft API description')
    .setVersion('1.0')
    .addApiKey({ type: 'apiKey', name: 'Authorisation', in: 'header' })
    .build();
