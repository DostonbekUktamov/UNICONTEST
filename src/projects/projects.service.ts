import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrganizationsService } from 'src/organizations/organizations.service';
import { Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';
import { Inject } from '@nestjs/common/decorators'
import { UsersService } from 'src/users/users.service';
import { ClientRequest } from 'http';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,

    @Inject(OrganizationsService)
    private organizationsService: OrganizationsService,

    @Inject(UsersService)
    private usersService: UsersService,
  ) { }

  async create(createProjectDto: CreateProjectDto) {

    const userFound = await this.usersService.findOne(
      createProjectDto.created_by
    );

    const org = await this.organizationsService.findOne(
      createProjectDto.created_by,
    );

    if (!Object.keys(userFound)[0])
      throw new BadRequestException('created_by id not found');

    if (!Object.keys(org)[0])
      throw new BadRequestException('org_id id not found');

    try {

      const newPro = await this.projectRepository
        .createQueryBuilder()
        .insert()
        .into('projects')
        .values({ ...createProjectDto })
        .returning('id')
        .execute();

      return { id: newPro.raw[0].id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findAll(): Promise<Project[]> {
    return this.projectRepository
      .createQueryBuilder('projects')
      .where('deleted_at = :deleted_at', { deleted_at: false })
      .getMany()
      ;
  }

  async findOne(id: number): Promise<{}> {
    const findPro = await this.projectRepository
      .createQueryBuilder('projects')
      .where('projects.id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .getOne();
    if (!findPro) return {};

    return findPro;
  }

  async update(
    id: number,
    updateProjectDto: UpdateProjectDto,
  ): Promise<{ id: number }> {
    const findPro = await this.findOne(id);

    const userFound = await this.usersService.findOne(
      updateProjectDto.created_by
    );

    const org = await this.organizationsService.findOne(
      updateProjectDto.created_by,
    );
    if (!findPro)
      throw new NotFoundException('Project with procet_id not exists!');

    if (!Object.keys(userFound)[0])
      throw new BadRequestException('created_by id not found');

    if (!Object.keys(org)[0])
      throw new BadRequestException('org_id id not found');


    try {
      await this.projectRepository
        .createQueryBuilder()
        .update('projects')
        .set({ ...updateProjectDto })
        .where('id = :id', { id })
        .andWhere('deleted_at = :deleted_at', { deleted_at: false })
        .execute();

      return { id };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async remove(id: number): Promise<{ id: number }> {
    const projectValid = await this.findOne(id);
    if (!Object.keys(projectValid)[0])
      throw new BadRequestException('projectId not found!');

    await this.projectRepository
      .createQueryBuilder()
      .update('projects')
      .set({ deleted_at: true })
      .where('id = :id', { id })
      .andWhere('deleted_at = :deleted_at', { deleted_at: false })
      .execute();
    return { id };
  }

  async projectWithTask() {
    return await this.projectRepository.query(`
    SELECT 
    p.id,
    p.org_id,
    p.created_by,
    JSON_AGG(DISTINCT ou.*) AS orgUser,
    JSON_AGG(DISTINCT t.*) AS task
    FROM 
    projects p 
    LEFT JOIN task t 
    ON p.id = t.project_id
    LEFT JOIN organization_user ou
    ON t.worker_user_id = ou.id
    GROUP BY p.id
    `);
  }
}
