import { Module } from '@nestjs/common';
import { OrganizationsUserService } from './organizations_user.service';
import { OrganizationsUserController } from './organizations_user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrganizationsUser } from './entities/organizations_user.entity';
import { Task } from 'src/tasks/entities/task.entity';
import { Project } from 'src/projects/entities/project.entity';
import { UsersModule } from 'src/users/users.module';
import { OrganizationsModule } from 'src/organizations/organizations.module';

@Module({
  imports: [TypeOrmModule.forFeature([OrganizationsUser, Task, Project]), UsersModule, OrganizationsModule],
  controllers: [OrganizationsUserController],
  providers: [OrganizationsUserService],
  exports: [OrganizationsUserService]
})
export class OrganizationsUserModule { }
