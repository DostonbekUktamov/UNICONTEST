import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { OrganizationsUserService } from './organizations_user.service';
import { CreateOrganizationsUserDto } from './dto/create-organizations_user.dto';
import { UpdateOrganizationsUserDto } from './dto/update-organizations_user.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from 'utils/roles/roles.guard';
import { Roles } from 'utils/roles/roles.decorator';
import { Role } from 'utils/roles/role.enum';

@UseGuards(RolesGuard)
// @Roles(Role.Employee)
@ApiTags('organizationsUser')
@Controller('organizations-user')
export class OrganizationsUserController {
  constructor(
    private readonly organizationsUserService: OrganizationsUserService,
  ) { }

  @Post()
  create(@Body() createOrganizationsUserDto: CreateOrganizationsUserDto) {
    return this.organizationsUserService.create(createOrganizationsUserDto);
  }

  @Get()
  findAll() {
    return this.organizationsUserService.findAll();
  }

  @Get('/get:id')
  findOne(@Param('id') id: string) {
    return this.organizationsUserService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrganizationsUserDto: UpdateOrganizationsUserDto,
  ) {
    return this.organizationsUserService.update(
      +id,
      updateOrganizationsUserDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.organizationsUserService.remove(+id);
  }


  @Roles(Role.Employee)
  @ApiOperation({ summary: "Shu xodimga biriktirilgan barcha vazifalarni statuslar kesmida ro'yxati" })
  @Get('/orgUserWithTask')
  orgUserWithTask() {
    return this.organizationsUserService.orgUserWithTask();
  }
}
