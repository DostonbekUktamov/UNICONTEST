import {
  Controller,
  Get,
  UseGuards
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Role } from 'utils/roles/role.enum';
import { Roles } from 'utils/roles/roles.decorator';
import { RolesGuard } from 'utils/roles/roles.guard';
import { StatisticsService } from './statistics.service';

@UseGuards(RolesGuard)
@Roles(Role.Admin)
@ApiTags('statistics')
@Controller('statistics')
export class StatisticsController {
  constructor(private readonly statisticsService: StatisticsService) { }

  @ApiOperation({ summary: "Tashkilot kesmida statistika: (tashkilot nomi, loyihalar soni, umumiy vazifalar soni)" })
  @Get('/orgStatistics')
  orgStatistics() {
    return this.statisticsService.OrgStatistics();
  }

  @ApiOperation({ summary: "Tashkilotning loyihalari kesmida: (tashkilot nomi, loyiha nomi, loyiha vazifalari soni)" })
  @Get('/proStatistics')
  proStatistics() {
    return this.statisticsService.projectStatistics();
  }


  @ApiOperation({ summary: "Umumiy statistika (umumiy tashkilotlar soni, umumiy loyihalar soni, umumiy vazifalar soni)" })
  @Get('/allStatistics')
  allStatistics() {
    return this.statisticsService.allStatistics();
  }
}
